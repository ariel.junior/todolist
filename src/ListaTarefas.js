// React
import React, { useEffect, useState } from 'react';

import instanciaAxios from "./ajax/instanciaAxios";




const ListaTarefas = () => { 

 const [listaTarefas, setListaTarefas] = useState ([]);
 const [listaCategorias, setListaCategorias] = useState ([]);
 const [listaTransportadora, setListaTransportadora] = useState ([]);
 const [listaDias, setListaDias] = useState ([]);
 const [listaTurnos, setListaTurnos] = useState ([]);
 


 const [descricaoNovoItem, setDescricaoNovoItem] = useState ('');
 const [categoriaNovoItem, setCategoriaNovoItem] = useState ('');
 const [transportadoraNovoItem, setTransportadoraNovoItem] = useState ('');
 const [diaNovoItem, setDiaNovoItem] = useState ('');
 const [turnoNovoItem, setTurnoNovoItem] = useState ('');
 const [alertaNovoItem, setAlertaNovoItem] = useState('desligado');



// todos os useEffects:

 useEffect(() => {
  pegarTarefas()
  pegarCategorias();
  pegarTransportadora()
  pegarDias()
  pegarTurnos()
  
  
  
}, [])
 
// todos os const pegar:

const pegarTarefas = async () => {


            try{

              const resposta = await instanciaAxios.get('../json/tarefas.json');
              setListaTarefas(resposta.data.tarefas);
          }    catch (error) {
              console.log(error.message);
              

            }   

          }; 



const pegarTransportadora = async () => {


            try{

              const resposta = await instanciaAxios.get('../json/transportadora.json');
              setListaTransportadora(resposta.data.transportadora);
          }    catch (error) {
              console.log(error.message);
              

            }   

          }; 





const pegarCategorias = async () => {


            try{

              const resposta = await instanciaAxios.get('../json/categorias.json');
              setListaCategorias(resposta.data.categorias);
            } catch (error) {
              console.log(error.message);
              

          }   

         }; 



const pegarDias = async () => {


            try{

              const resposta = await instanciaAxios.get('../json/dias.json');
              setListaDias(resposta.data.dias);
          }   catch (error) {
              console.log(error.message);
              

            }   

          }; 

const pegarTurnos = async () => {


            try{

              const resposta = await instanciaAxios.get('../json/turnos.json');
              setListaTurnos(resposta.data.turnos);
          }   catch (error) {
              console.log(error.message);
              

            }   

          }; 




// todos os const Opcoes Componentes:

const OpcoesCategoriasComponente = () => {
 
              if (listaCategorias.length > 0 ) {
              const listaCategoriasJSX = listaCategorias.map( ( item ) => {

                return (
                  <option key={ item.id }
                  value={item.id} >
                      { item.descricao }
                  </option>

              )

            })

              return listaCategoriasJSX
            }  else {
              return null  
            }
            }





 const OpcoesTransportadoraComponente = () => { 
  
              if(listaTransportadora.length > 0 ) { 
              const listaTransportadoraJSX = listaTransportadora.map ( ( item ) => {
                  
                  return (
                    <option key={ item.id }
                    value={item.id} >
                      { item.descricao}
                    </option>
                  
            )

            })
              
              return listaTransportadoraJSX;
            }  else {
              return null  

            }  
            }



  const OpcoesDiasComponente = () => { 


               if(listaDias.length > 0 ) { 
               const listaDiasJSX = listaDias.map ( ( item ) => {
                      
                return (
                <div key={item.id}>
                <input 
                type='radio'
                name='dia' 
                value={ item.id} 
                id={`dia-${item.valor}` }
                onChange={ (evento)  => setDiaNovoItem( evento.target.value) } 
                checked={item.id === diaNovoItem} />
                <label htmlFor={ `dia-${item.valor}` } >
                { item.rotulo }
                </label>
                </div>

              )
                  
             })
                    
                 return listaDiasJSX;
                }  else {
                  return null    

              }
              }

  const OpcoesTurnosComponente = () => {
 
            if(listaTurnos.length > 0 ) {
            const listaTurnosJSX = listaTurnos.map( ( item  ) => {
      
            return (
            <div key={item.id}>
            <input 
            type='radio'
            name='turno' 
            value={ item.id } 
            id={`turno-${item.valor}` }
            onChange={ (evento)  => setTurnoNovoItem( evento.target.value) } 
            checked={item.id === turnoNovoItem} />
            <label htmlFor={ `turno-${item.valor}` }>
            { item.rotulo }
            </label>
            </div>
      
          )
      
              })
              
                return listaTurnosJSX;
              }  else {
                return null  
                
            }
            }

  const AlertaIconeComponente = () => {
    return (
      <img src='/image/relogioalarme.png' alt="alerta" style={ { "width": '20px', "marginLeft": '15px' } } />
    )
  }

  const CorpoTabelaComponente = () => {
    
          if(listaTarefas.length > -1) {
          return (
          <tbody>
          { listaTarefas.map( ( item ) =>  {
          return (
          <LinhaTabelaComponente 
          key={ item.id }
          id={ item.id }
          descricao={ item.descricao} 
          idTransportadora={ item.idTransportadora} 
          idCategoria={ item.idCategoria} 
          idDia={item.idDia}
          alerta={item.alerta} 
          idTurno={item.idTurno} 
          />
          
                )
              } 
            ) 
            }
              </tbody>      

            )
          } else {
            return null 
          }
          } 


  
  const LinhaTabelaComponente = ( props ) => {

 
 
    let _categoria 
    _categoria = listaCategorias ? listaCategorias.find( item  => { 
      console.log(listaCategorias);
      return item.id === props.idCategoria; 

    }): _categoria 



    let _transportadora
    _transportadora = listaTransportadora ? listaTransportadora.find( item  => { 
      console.log(listaTransportadora);
      return item.id === props.idTransportadora; 

    }): _transportadora


   
    
    let _dia
    _dia = listaDias ? listaDias.find( item  => { 
     console.log(listaDias);
     return item.id === props.idDia;  

   }): _dia


   
   let _turno
    _turno = listaTurnos ? listaTurnos.find( item  => { 
     console.log(listaTurnos);
     return item.id === props.idTurno;  

   }): _turno


   const _alerta = props.alerta === 'ligado' ? <AlertaIconeComponente /> : null 
   
  
     return (
      <tr bgcolor="#DEEDE5" >
          <td>
          { props.descricao }
          {_alerta}
          </td>
          <td>{ _transportadora ?_transportadora.descricao : "não tem trasnportadora" }</td>
          <td>{ _categoria ? _categoria.descricao :"não tem categoria"}</td> 
          <td>{ _dia ? _dia.rotulo :"não tem dia"}</td> 
          <td>{ _turno ? _turno.rotulo :"não tem turno"}</td> 
          <td>
            <img id="excluir" 
            src='/image/excluir.png' alt="excluir" style={ { "width": '15px', "marginLeft": '15px' } } 
            onClick={ () => { removerItem( props.id ) } }
            />
          </td>  
          
          
     </tr>  

    )

  }


  const incluirItem =  () => {

     console.log( 'o usuario clicou no botão incluir' )

    if( categoriaNovoItem > 0 && descricaoNovoItem  ) {


  
    
   
    const indiceUltimoElemento = listaTarefas.length -1
    const ultimoElemento = listaTarefas[ indiceUltimoElemento ]
    const idUltimoElemento = ultimoElemento.id
    const idNovoItem = parseInt( idUltimoElemento) + 1
    console.log(idNovoItem)

    const novoItem = {
      "id": idNovoItem,
      "descricao": descricaoNovoItem,
      "idTransportadora": transportadoraNovoItem,
      "idCategoria": categoriaNovoItem,
      "idDia": diaNovoItem,
      "idTurno": turnoNovoItem

    }

    setListaTarefas( [...listaTarefas, novoItem] )
  }else {
   alert('Por favor, preencha todos os campos.')

  }     

  }



  const removerItem = ( idSelecionado ) => {

    console.log(`O id selecionado foi: ${idSelecionado}`)

    const _listaTarefas = listaTarefas.filter( (item) => {
      return item.id !== idSelecionado
    })

    setListaTarefas( _listaTarefas )
  }

  return (

   
                        
    <>
               {/*ReactFragment*/} 


 
      <div id="container"> 
                   <div id="lado-esquerdo">
                                      <h1>Novo Item:</h1>
                                      <div className="campo-descricao">
                                      <p><label>Descrição:</label></p>  
                                      <input id="tipo-descricao" type="text" 
                                       value={descricaoNovoItem}
                                       onChange={ (evento) => setDescricaoNovoItem 
                                       ( evento.target.value ) } />
                                                    
                                       </div>
                                                

                                   <div className="campo-transportadora">
                                        <p> <label>Transportadora:</label></p>
                                         <select id="tipo-transportadora"
                                         value={transportadoraNovoItem}
                                          onChange={ (evento) => setTransportadoraNovoItem
                                          (evento.target.value) } >
                                          <OpcoesTransportadoraComponente /> 
                                           </select>
                                                
                                   </div>
                                                
                                    <div className="campo-transportadora">
                                        <p> <label>Categoria:</label> </p>
                                           <select id="tipo-categoria"
                                           value={categoriaNovoItem}
                                            onChange={ (evento) => setCategoriaNovoItem
                                           (evento.target.value) } > 
                                            <OpcoesCategoriasComponente />
                                            </select>
                                                
                                     </div>

                                               
                                      <div className= "campo-dia">
                                            <p><label>Dia:</label></p>

                                             <OpcoesDiasComponente/> 
                                      </div>
                                           
                                                  
                                       <div className="campo-turno">
                                             <p> <label>Turno:</label></p>

                                              <OpcoesTurnosComponente />
                                       </div>

                                       <div className="alarme">
                                         <p>Alerta?</p>  
                                         
                                         <input type="checkbox" id="campo-alerta"  name="campo-alerta"
                                         onChange={ () => { setAlertaNovoItem ( alertaNovoItem === "ligado" ?
                                         "desligado" : "ligado" ) } } />
                                         <label htmlFor="campo-alerta">Ligado</label>
                                       </div>

                                               

                                             <button id="botao-incluir"
                                             onClick={ () => incluirItem()  }
                                          
                                             >Incluir</button>
                                           
                        </div> 
                                  <div id="lado-direito">

                                        <div id="texto-chamativo">
                                                <h2>Recebimento Inteligente:</h2>

                                               </div>

                                                <table>

                                        
                                                  <thead >
                                                  
                                                    <tr  bgcolor="#e7dc40" > 
                                                        <th>DESCRIÇÃO</th>  
                                                        <th>TRANSPORTADORA</th> 
                                                        <th>CATEGORIA</th> 
                                                        <th>DIA</th> 
                                                        <th>TURNO</th> 
                                                        <th>AÇÕES</th> 
                                                  
                                                    </tr>
                                                  
                                                  </thead>

                                                  <CorpoTabelaComponente />
                                            
                                                
                                                      <tfoot>

                                                  
                                                          <tr>
                                                          <td colSpan="6">Total de itens: { listaTarefas.length}  </td>        
                                                          </tr>      
                                                      </tfoot>    

                                                      </table>  
                                              

                                </div>
                                                 
                                            
                                             
               </div>     
                           

           </>            
         
  

  ) 
  
}
  
  

 
export default ListaTarefas;